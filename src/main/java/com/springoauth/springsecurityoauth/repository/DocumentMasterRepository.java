package com.springoauth.springsecurityoauth.repository;

import com.springoauth.springsecurityoauth.models.DocumentMaster;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DocumentMasterRepository extends MongoRepository<DocumentMaster,String> {
    public DocumentMaster findByName(String name);
}
