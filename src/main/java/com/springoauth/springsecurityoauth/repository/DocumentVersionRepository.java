package com.springoauth.springsecurityoauth.repository;

import com.springoauth.springsecurityoauth.models.DocumentVersion;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DocumentVersionRepository extends MongoRepository<DocumentVersion,String> {
}
