package com.springoauth.springsecurityoauth.repository;

import com.springoauth.springsecurityoauth.models.DocumentChild;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DocumentChildRepository extends MongoRepository<DocumentChild,String> {
}
