package com.springoauth.springsecurityoauth.service;

import com.springoauth.springsecurityoauth.models.DocumentMaster;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface DocumentMasterService {
    public DocumentMaster getById(String id);
    public DocumentMaster saveDocumentMaster(DocumentMaster dm, MultipartFile[] multipartFile);
    public byte[] getFiles(String id) throws IOException;
}
