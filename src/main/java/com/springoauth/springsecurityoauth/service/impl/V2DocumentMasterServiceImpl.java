package com.springoauth.springsecurityoauth.service.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.springoauth.springsecurityoauth.models.DocumentChild;
import com.springoauth.springsecurityoauth.models.DocumentMaster;
import com.springoauth.springsecurityoauth.models.DocumentVersion;
import com.springoauth.springsecurityoauth.repository.DocumentChildRepository;
import com.springoauth.springsecurityoauth.repository.DocumentMasterRepository;
import com.springoauth.springsecurityoauth.repository.DocumentVersionRepository;
import com.springoauth.springsecurityoauth.service.DocumentMasterService;
import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service("versionTwo")
public class V2DocumentMasterServiceImpl implements DocumentMasterService {

    private DocumentMasterRepository documentMasterRepository;
    private DocumentChildRepository documentChildRepository;
    private DocumentVersionRepository documentVersionRepository;
    private GridFsTemplate gridFsTemplate;
    private GridFsOperations gridFsOperations;
    private GridFSBucket gridFSBucket;

    public V2DocumentMasterServiceImpl(DocumentMasterRepository documentMasterRepository, DocumentChildRepository documentChildRepository, DocumentVersionRepository documentVersionRepository, GridFsTemplate gridFsTemplate, GridFsOperations gridFsOperations,GridFSBucket gridFSBucket) {
        this.documentMasterRepository = documentMasterRepository;
        this.documentChildRepository = documentChildRepository;
        this.documentVersionRepository = documentVersionRepository;
        this.gridFsTemplate = gridFsTemplate;
        this.gridFsOperations = gridFsOperations;
        this.gridFSBucket = gridFSBucket;
    }

    @Override
    public DocumentMaster getById(String id) {
        System.out.println("From second impl ... ");
        try {
            GridFSFile gridFSFile = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(id)));
            DocumentChild documentChild = new DocumentChild();
            documentChild.setInputStream(gridFsOperations.getResource(gridFSFile).getInputStream());
            DocumentMaster documentMaster = new DocumentMaster();
            List<DocumentChild> documentChildList = new ArrayList<>();
            documentChildList.add(documentChild);
            documentMaster.setDocumentChildList(documentChildList);
            return documentMaster;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public DocumentMaster saveDocumentMaster(DocumentMaster dm, MultipartFile[] multipartFile) {
        System.out.println("from second implementation ... ");
        DBObject dbObject = new BasicDBObject();
        try {
            List<DocumentChild> documentChildList = new ArrayList<>();
            for (MultipartFile file: multipartFile) {
                List<DocumentVersion> documentVersionList = new ArrayList<>();
                ObjectId id = gridFsTemplate.store(file.getInputStream(),file.getName(),file.getContentType(),dbObject);
                DocumentChild documentChild = new DocumentChild();
                documentChild.setFilesId(id.toString());
                documentChild.setDocumentVersionList(documentVersionList);
                documentChildRepository.save(documentChild);
                documentChildList.add(documentChild);
            }
            dm = new DocumentMaster("tags1","name1");
            dm.setDocumentChildList(documentChildList);
            documentMasterRepository.save(dm);
            return dm;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public byte[] getFiles(String id) throws IOException {
        GridFSFile file = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(id)));
        GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(file.getObjectId());
        GridFsResource gridFsResource = new GridFsResource(file,gridFSDownloadStream);
        return IOUtils.toByteArray(gridFsResource.getInputStream());
    }
}
