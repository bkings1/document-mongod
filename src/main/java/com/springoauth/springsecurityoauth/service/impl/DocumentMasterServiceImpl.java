package com.springoauth.springsecurityoauth.service.impl;

import com.springoauth.springsecurityoauth.models.DocumentChild;
import com.springoauth.springsecurityoauth.models.DocumentMaster;
import com.springoauth.springsecurityoauth.models.DocumentVersion;
import com.springoauth.springsecurityoauth.repository.DocumentChildRepository;
import com.springoauth.springsecurityoauth.repository.DocumentMasterRepository;
import com.springoauth.springsecurityoauth.repository.DocumentVersionRepository;
import com.springoauth.springsecurityoauth.service.DocumentMasterService;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("versionOne")
public class DocumentMasterServiceImpl implements DocumentMasterService {

    private DocumentMasterRepository documentMasterRepository;
    private DocumentChildRepository documentChildRepository;
    private DocumentVersionRepository documentVersionRepository;

    public DocumentMasterServiceImpl(DocumentMasterRepository documentMasterRepository,DocumentChildRepository documentChildRepository,DocumentVersionRepository documentVersionRepository) {
        this.documentMasterRepository = documentMasterRepository;
        this.documentChildRepository = documentChildRepository;
        this.documentVersionRepository = documentVersionRepository;
    }

    @Override
    public DocumentMaster getById(String id) {
        return documentMasterRepository.findById(id).get();
    }

    @Override
    public DocumentMaster saveDocumentMaster(DocumentMaster dm, MultipartFile[] multipartFiles) {
        try {
            List<DocumentChild> documentChildList = new ArrayList<>();
            for (MultipartFile file: multipartFiles) {
                List<DocumentVersion> documentVersionList = new ArrayList<>();
                DocumentVersion documentVersion = new DocumentVersion();
                documentVersion.setFileLocation("myFileLocation");
                documentVersion.setOriginalFileName("org file name");
                documentVersion.setVersionNumber(1l);
                documentVersionRepository.save(documentVersion);
                DocumentChild documentChild = new DocumentChild();
//                documentChild.setFile(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
                documentVersionList.add(documentVersion);
                documentChild.setDocumentVersionList(documentVersionList);
                documentChildRepository.save(documentChild);
                documentChildList.add(documentChild);
            }
            dm.setDocumentChildList(documentChildList);
            DocumentMaster documentMaster = documentMasterRepository.save(dm);
            return documentMaster;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public byte[] getFiles(String id) {
        return new byte[0];
    }
}
