package com.springoauth.springsecurityoauth.controller;

import com.mongodb.client.gridfs.model.GridFSFile;
import com.springoauth.springsecurityoauth.models.DocumentMaster;
import com.springoauth.springsecurityoauth.service.DocumentMasterService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/api/mongo/document-master")
public class DocumentMasterController {

    private DocumentMasterService documentMasterService;
    private DocumentMasterService documentMasterServiceTwo;

    public DocumentMasterController(@Qualifier("versionOne") DocumentMasterService documentMasterService,@Qualifier("versionTwo") DocumentMasterService documentMasterServiceTwo) {
        this.documentMasterService = documentMasterService;
        this.documentMasterServiceTwo = documentMasterServiceTwo;
    }

    @GetMapping("/{id}")
    public String get(@PathVariable String id, Model model) {
        DocumentMaster documentMaster = documentMasterService.getById(id);
        model.addAttribute("tags",documentMaster.getTags());
        List allImages = new ArrayList(),result = new ArrayList();
        allImages = documentMaster.getDocumentChildList();
        model.addAttribute("fileListSize",allImages.size());
        for(int i=0;i<allImages.size();i++) {
//            result.add(Base64.getEncoder().encodeToString(documentMaster.getDocumentChildList().get(i).getFile().getData()));
        }
        model.addAttribute("result",result);
        Map map = new HashMap();
        map.put("id",documentMaster.getId());
//        map.put("myFile", Base64.getEncoder().encodeToString(documentMaster.getDocumentChildList().get(0).getFile().getData()));
        return "view";
    }

    @PostMapping(path = "/undo",consumes = "multipart/form-data")
    public String saveDM(@ModelAttribute DocumentMaster documentMasterBody, @RequestParam MultipartFile[] multipartFile) {
        DocumentMaster documentMaster =documentMasterService.saveDocumentMaster(documentMasterBody,multipartFile);
        return "redirect:/api/mongo/document-master/" + documentMaster.getId();
//        return "view";
    }

    @GetMapping("/large/{id}")
    public String getLarge(@PathVariable String id,Model model) {
        DocumentMaster documentMaster = documentMasterServiceTwo.getById(id);
        model.addAttribute("inputStream",documentMaster.getDocumentChildList().get(0).getInputStream());
        System.out.println("inputstream " + documentMaster.getDocumentChildList().get(0).getInputStream());
        model.addAttribute("url", id);
        return "view";
    }

    @GetMapping("/large/download/{id}")
    @ResponseBody
    public byte[] getFile(@PathVariable String id) throws IOException {
        System.out.println("Inside download api ... ");
        return documentMasterServiceTwo.getFiles(id);
    }

    @PostMapping(consumes = "multipart/form-data")
    public String saveFromSecondImpl(@ModelAttribute DocumentMaster documentMasterBody, @RequestParam MultipartFile[] multipartFile) {
        System.out.println("second controller");
        DocumentMaster documentMaster = documentMasterServiceTwo.saveDocumentMaster(documentMasterBody,multipartFile);
        return "redirect:/api/mongo/document-master/large/" + documentMaster.getDocumentChildList().get(0).getFilesId();
    }
}
