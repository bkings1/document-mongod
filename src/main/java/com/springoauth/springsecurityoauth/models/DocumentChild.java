package com.springoauth.springsecurityoauth.models;

import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.InputStream;
import java.util.List;

@Document(collection = "document_child")
public class DocumentChild {
    @Id
    private String id;
//    private Binary file;
    private InputStream inputStream;
    private String filesId;
    private List<DocumentVersion> documentVersionList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public List<DocumentVersion> getDocumentVersionList() {
        return documentVersionList;
    }

    public void setDocumentVersionList(List<DocumentVersion> documentVersionList) {
        this.documentVersionList = documentVersionList;
    }

    public String getFilesId() {
        return filesId;
    }

    public void setFilesId(String filesId) {
        this.filesId = filesId;
    }
}
