package com.springoauth.springsecurityoauth.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "document_master")
public class DocumentMaster {
    @Id
    private String id;
    private String tags;
    private String name;
    private List<DocumentChild> documentChildList;

    public DocumentMaster() {
    }

    public DocumentMaster(String tags,String name) {
        this.tags = tags;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DocumentChild> getDocumentChildList() {
        return documentChildList;
    }

    public void setDocumentChildList(List<DocumentChild> documentChildList) {
        this.documentChildList = documentChildList;
    }
}
